package Tugas1;

import java.util.Scanner;

public class MembalikkanKalimat {

    public static void main(String args[])
    {
        String kalimat, inverted = "";
        Scanner kb = new Scanner(System.in);
        System.out.print("Masukkan kalimat: ");
        kalimat = kb.nextLine();
        kb.close();
        for ( int i = kalimat.length() - 1; i >= 0; i-- )
        {
            inverted = inverted + kalimat.charAt(i);
        }
        System.out.println("Kalimat terbalik : " + inverted);
    }
}
