package Tugas1;


public class Pr2_Segitiga_Rata_Kiri {

    public static void main(String[] args) {

        int x = 10;
        for (int i = 1; i <= x; i++){
            for (int j = 9; j >= i; j--) {
                System.out.print(' ');
            }
            for (int k = 1; k < i; k++){
                System.out.print('@');
            }
            System.out.println();
        }
    }
}
