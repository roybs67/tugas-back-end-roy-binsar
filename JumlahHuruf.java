package Tugas1;

import java.util.Scanner;

public class JumlahHuruf {
    public static void main(String[] args) {


        Scanner kb = new Scanner(System.in);
        System.out.print("Variable : ");
        int jmlhHuruf = 1;
        String kalimat = kb.nextLine().toLowerCase();
        kb.close();
        char[] arrHuruf = kalimat.toCharArray();
        for(int i = 0; i < arrHuruf.length; i++){
            for(int j = 0; j < arrHuruf.length; j++){
                if(arrHuruf[i]!=' '){
                    if(i!=j&&(arrHuruf[i]==arrHuruf[j])){
                        jmlhHuruf++;
                        arrHuruf[j]=' ';
                    }
                    if(j==arrHuruf.length-1){
                        System.out.println("Alfabet '"+arrHuruf[i]+"' muncul "+jmlhHuruf+" kali");
                        jmlhHuruf=1;
                    }
                }
            }
        }
    }
}
